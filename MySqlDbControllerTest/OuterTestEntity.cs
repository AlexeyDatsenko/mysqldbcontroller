﻿using MySqlDbController;

namespace MySqlDbControllerTest
{
    [Table("test.inner_test_entity")]
    class OuterTestEntity
    {
        [Column("entity_id", isPrimaryKeyColumn: true, isGeneratedValueColumn: true)]
        [ColumnSettings(ColumnDataType.BIGINT, autoIncrement: true, primaryKey: true)]
        public long EntityId { get; set; }

        [Column("name")]
        [ColumnSettings(ColumnDataType.VARCHAR, dataTypeLenght: 45, notNull: true)]
        public string Name { get; set; }

        [Column("sex", isEnumObject: true, enumType: typeof(PersonSex))]
        [ColumnSettings(ColumnDataType.INT, defaultValue: 0, notNull: true)]
        public PersonSex Sex { get; set; }

        public OuterTestEntity() { }

        public OuterTestEntity(long entityId, string name, PersonSex sex)
        {
            EntityId = entityId;
            Name = name;
            Sex = sex;
        }
    }
}
