﻿using MySqlDbController;

namespace MySqlDbControllerTest
{
    [Table("test.outer_test_table")]
    class InnerTestEntity
    {
        [Column("entity_id", isPrimaryKeyColumn: true, isGeneratedValueColumn: true)]
        public int EntityId { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("age")]
        public int Age { get; set; }

        [Column("sex", isEnumObject: true, enumType: typeof(PersonSex))]
        public PersonSex Sex { get; set; }

        public InnerTestEntity() { }

        public InnerTestEntity(string name, int age, PersonSex sex)
        {
            Name = name;
            Age = age;
            Sex = sex;
        }
    }

    public enum PersonSex
    {
        MALE = 0,
        FEMALE = 1
    };
}
