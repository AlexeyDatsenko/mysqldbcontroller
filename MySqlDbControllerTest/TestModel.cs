﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySqlDbController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySqlDbControllerTest
{
    [TestClass]
    public class TestModel
    {
        private readonly ModelBasedDbController<InnerTestEntity> innerEntityController = new ModelBasedDbController<InnerTestEntity>("localhost", "user", "123456");
        private readonly ModelBasedDbController<OuterTestEntity> outerEntityController = new ModelBasedDbController<OuterTestEntity>("localhost", "user", "123456");

        [TestMethod]
        public void TestSelectAllInnerEntity()
        {
            var selectResult = innerEntityController.Select();
            Assert.AreNotEqual(0, selectResult.Count);
        }

        [TestMethod]
        public void TestSelectByIdInnerEntity()
        {
            var selectResult = innerEntityController.SelectById(1, "entity_id");
            Assert.IsNotNull(selectResult);
        }

        [TestMethod]
        public void TestInsertInnerEntity()
        {
            innerEntityController.Insert(new InnerTestEntity("Петр", 100, PersonSex.MALE));
        }

        [TestMethod]
        public void TestUpdateInnerEntity()
        {
            var entity = innerEntityController.SelectById(1, "entity_id");
            entity.Name = "Иван Новый";
            entity.Age = 500;
            innerEntityController.Update(entity);
        }

        [TestMethod]
        public void TestDeleteInnerEntity()
        {
            var count = innerEntityController.DeleteById(2, "entity_id");
            Assert.AreNotEqual(0, count);
        }

        [TestMethod]
        public void TestCreateTable()
        {
            outerEntityController.CreateTableIfNotExists();
        }

        [TestMethod]
        public void TestCreateClassFile()
        {
            outerEntityController.CreateClassFile(@"C:\files\test_class_file.cs", "test.outer_test_table", "test_namespace", true);
        }
    }
}
