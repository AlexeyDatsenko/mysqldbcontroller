﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MySqlDbController
{
    /// <summary>
    /// Controller for interacting with MySQL Server
    /// </summary>
    /// <typeparam name="Entity">Entity</typeparam>
    public class ModelBasedDbController<Entity> : RootDbController where Entity : new()
    {
        private string TableAddress { get; }

        /// <summary>
        /// Manual constructor
        /// </summary>
        /// <param name="host">Server address</param>
        /// <param name="login">Username</param>
        /// <param name="password">Password</param>
        public ModelBasedDbController(
            string host,
            string login,
            string password) : base(host, login, password)
        {
            TableAddress = ((Table)Attribute.GetCustomAttributes(typeof(Entity)).ToList().Find(x => x.GetType() == typeof(Table))).TableAddress;
        }

        /// <summary>
        /// MySQL constructor
        /// </summary>
        /// <param name="mySqlConnection">MySQL connection</param>
        public ModelBasedDbController(MySqlConnection mySqlConnection) : base(mySqlConnection)
        {
            TableAddress = ((Table)Attribute.GetCustomAttributes(typeof(Entity)).ToList().Find(x => x.GetType() == typeof(Table))).TableAddress;
        }

        /// <summary>
        /// Constructor from settings object
        /// </summary>
        /// <param name="connectionSettings">Connection parameters</param>
        public ModelBasedDbController(ConnectionSettings connectionSettings) : base(connectionSettings) 
        {
            TableAddress = ((Table)Attribute.GetCustomAttributes(typeof(Entity)).ToList().Find(x => x.GetType() == typeof(Table))).TableAddress;
        }

        /// <summary>
        /// Constructor from file
        /// </summary>
        /// <param name="connectionName">Connection identifier</param>
        /// <param name="connectionFilePath">File path</param>
        public ModelBasedDbController(string connectionName, string connectionFilePath) : base(connectionName, connectionFilePath)
        {
            TableAddress = ((Table)Attribute.GetCustomAttributes(typeof(Entity)).ToList().Find(x => x.GetType() == typeof(Table))).TableAddress;
        }

        /// <summary>
        /// Creates a table in the specified DB based on model attributes
        /// </summary>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="Exception">Error of extracting attributes</exception>"
        public void CreateTableIfNotExists()
        {
            var type = typeof(Entity);
            var propertyList = typeof(Entity).GetProperties().ToList();
            var propertyColumnInfoDictionary = new Dictionary<Column, ColumnSettings>();
            var parameterList = new List<MySqlParameter>();

            foreach (var property in propertyList)
            {
                var propertyColumnList = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));
                var propertyColumnSettingsList = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(ColumnSettings));

                if (propertyColumnList.Count == 0 || propertyColumnSettingsList.Count == 0)
                    continue;

                propertyColumnInfoDictionary.Add((Column)propertyColumnList[0], (ColumnSettings)propertyColumnSettingsList[0]);
            }

            if (propertyColumnInfoDictionary.Count == 0)
                throw new ArgumentNullException("Could not find described columns for table");

            var commandStringBuilder = new StringBuilder();
            commandStringBuilder
                .Append("CREATE TABLE IF NOT EXISTS ")
                .Append(TableAddress)
                .Append("(");

            for (var i = 0; i < propertyColumnInfoDictionary.Count; i++)
            {
                var propertyColumnInfo = propertyColumnInfoDictionary.ElementAt(i);

                if (propertyColumnInfo.Key.IsEnumObject && propertyColumnInfo.Value.DataType != ColumnDataType.INT && propertyColumnInfo.Value.DataType != ColumnDataType.TEXT)
                    throw new ArgumentException("Для хранения значений enum столбец должен иметь тип INT или TEXT");

                commandStringBuilder.Append(propertyColumnInfo.Key.Name).Append(" ").Append(propertyColumnInfo.Value.DataType.ToString());

                if (propertyColumnInfo.Value.DataTypeLenght != 0)
                {
                    var guid = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    commandStringBuilder.Append("(@").Append(guid).Append(")");
                    parameterList.Add(new MySqlParameter(guid, propertyColumnInfo.Value.DataTypeLenght));
                }

                if (propertyColumnInfo.Value.NotNull)
                    commandStringBuilder.Append(" NOT NULL");

                if (propertyColumnInfo.Value.DefaultValue != null)
                {
                    var guid = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    commandStringBuilder.Append(" DEFAULT @").Append(guid);
                    parameterList.Add(new MySqlParameter(guid, propertyColumnInfo.Value.DefaultValue));
                }

                if (propertyColumnInfo.Value.AutoIncrement)
                    commandStringBuilder.Append(" AUTO_INCREMENT");

                if (propertyColumnInfo.Value.PrimaryKey)
                    commandStringBuilder.Append(" PRIMARY KEY");

                if (i != propertyColumnInfoDictionary.Count - 1)
                    commandStringBuilder.Append(",");
            }

            commandStringBuilder.Append(")");

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandStringBuilder.ToString(), connection);

                command.Parameters.AddRange(parameterList.ToArray());

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Removes a corteges by "where string" from a table asynchronously
        /// </summary>
        /// <param name="whereString">String for WHERE query parameter (like "moved=1 AND character LIKE 'super'")</param>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(string whereString, CancellationToken cancellationToken, int timeOutSeconds = 30)
        {
            var commandText = "DELETE FROM " + TableAddress + " " + whereString;
            var count = 0;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    await connection.OpenAsync(cancellationToken);
                    count = await command.ExecuteNonQueryAsync(cancellationToken);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return count;
        }
        
        /// <summary>
        /// Removes a corteges by "where string" from a table
        /// </summary>
        /// <param name="whereString">String for WHERE query parameter (like "moved=1 AND character LIKE 'super'")</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns></returns>
        public int Delete(string whereString, int timeOutSeconds = 30)
        {
            var commandText = "DELETE FROM " + TableAddress + " " + whereString;
            var count = 0;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    connection.Open();
                    count = command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return count;
        }

        /// <summary>
        /// Removes a cortege by identifier from a table
        /// </summary>
        /// <param name="id">ID of entity</param>
        /// <param name="idName">Name of identifier column</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>Number of corteges deleted (> 1 if there are external keys)</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="Exception">Error of extracting attributes</exception>
        public int DeleteById(object id, string idName = "id", int timeOutSeconds = 30)
        {
            var commandTextStringBuilder = new StringBuilder("DELETE FROM " + TableAddress + " WHERE " + idName);

            if (id.GetType() == typeof(string))
                commandTextStringBuilder.Append(" LIKE @").Append(idName);
            else
                commandTextStringBuilder.Append("=@").Append(idName);

            var count = 0;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandTextStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                command.Parameters.Add(new MySqlParameter(idName, id));

                try
                {
                    connection.Open();
                    count = command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return count;
        }

        /// <summary>
        /// Retrieves corteges from a table
        /// </summary>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="limit">Line limit</param>
        /// <param name="offset">Select offset</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <param name="columns">Columns to get (default is null, i.e. all)</param>
        /// <param name="whereString">String for WHERE query parameter (like "moved=1 AND character LIKE 'super'")</param>
        /// <param name="grouppingColumn">Column for grouping</param>
        /// <param name="orderByColumn">Sort column</param>
        /// <param name="desc">Sort descending flag</param>
        /// <returns>Corteges list</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="Exception">Error of extracting attributes</exception>"
        public async Task<List<Entity>> SelectAsync(
            CancellationToken cancellationToken,
            ulong limit = 10000, 
            ulong offset = 0,
            int timeOutSeconds = 30,
            string[] columns = null, 
            string whereString = null,
            string grouppingColumn = null, 
            string orderByColumn = null, 
            bool desc = false)
        {
            var commandTextStringBuilder = new StringBuilder("SELECT ");

            if (columns == null)
                commandTextStringBuilder.Append("*");
            else
                commandTextStringBuilder.Append(BuildAttributesSelectString(columns));

            commandTextStringBuilder.Append(" FROM ").Append(TableAddress);

            if (whereString != null)
                commandTextStringBuilder.Append(" WHERE ").Append(whereString);

            if (grouppingColumn != null)
                commandTextStringBuilder.Append(" GROUP BY ").Append(grouppingColumn);

            if (orderByColumn != null)
                commandTextStringBuilder.Append(" ORDER BY ").Append(orderByColumn);

            if (desc)
                commandTextStringBuilder.Append(" DESC");

            commandTextStringBuilder.Append(" LIMIT ").Append(offset.ToString()).Append(",").Append(limit.ToString());

            List<Entity> entities = null;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandTextStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    await connection.OpenAsync(cancellationToken);
                    var dataReader = await command.ExecuteReaderAsync(cancellationToken);
                    entities = BindEntityDataList(dataReader, columns);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return entities;
        }
        
        /// <summary>
        /// Retrieves corteges from a table
        /// </summary>
        /// <param name="limit">Line limit</param>
        /// <param name="offset">Select offset</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <param name="columns">Columns to get (default is null, i.e. all)</param>
        /// <param name="whereString">String for WHERE query parameter (like "moved=1 AND character LIKE 'super'")</param>
        /// <param name="grouppingColumn">Column for grouping</param>
        /// <param name="orderByColumn">Sort column</param>
        /// <param name="desc">Sort descending flag</param>
        /// <returns>Corteges list</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="Exception">Error of extracting attributes</exception>"
        public List<Entity> Select(
            ulong limit = 10000, 
            ulong offset = 0,
            int timeOutSeconds = 30,
            string[] columns = null, 
            string whereString = null,
            string grouppingColumn = null, 
            string orderByColumn = null, 
            bool desc = false)
        {
            var commandTextStringBuilder = new StringBuilder("SELECT ");

            if (columns == null)
                commandTextStringBuilder.Append("*");
            else
                commandTextStringBuilder.Append(BuildAttributesSelectString(columns));

            commandTextStringBuilder.Append(" FROM ").Append(TableAddress);

            if (whereString != null)
                commandTextStringBuilder.Append(" WHERE ").Append(whereString);

            if (grouppingColumn != null)
                commandTextStringBuilder.Append(" GROUP BY ").Append(grouppingColumn);

            if (orderByColumn != null)
                commandTextStringBuilder.Append(" ORDER BY ").Append(orderByColumn);

            if (desc)
                commandTextStringBuilder.Append(" DESC");

            commandTextStringBuilder.Append(" LIMIT ").Append(offset.ToString()).Append(",").Append(limit.ToString());

            List<Entity> entities = null;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandTextStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    connection.Open();
                    var dataReader = command.ExecuteReader();
                    entities = BindEntityDataList(dataReader, columns);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return entities;
        }

        /// <summary>
        /// Gets a cortege from a table by its identifier
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <param name="idName">Name of identifier column</param>
        /// <param name="columns">Columns to get</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>Cortege</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="Exception">Error of extracting attributes</exception>
        public Entity SelectById(object id, string idName = "id", string[] columns = null, int timeOutSeconds = 30)
        {
            var commandTextStringBuilder = new StringBuilder("SELECT ");

            if (columns == null)
                commandTextStringBuilder.Append("*");
            else
                commandTextStringBuilder.Append(BuildAttributesSelectString(columns));

            commandTextStringBuilder.Append(" FROM " + TableAddress + " WHERE " + idName);

            if (id.GetType() == typeof(string))
                commandTextStringBuilder.Append(" LIKE @").Append(idName);
            else
                commandTextStringBuilder.Append("=@").Append(idName);

            Entity entity = default;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandTextStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                command.Parameters.Add(new MySqlParameter(idName, id));

                try
                {
                    connection.Open();
                    var dataReader = command.ExecuteReader();
                    entity = BindEntityData(dataReader, columns);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return entity;
        }

        /// <summary>
        /// Adds a new record to the table.
        /// </summary>
        /// <param name="cortege">Object to add</param>
        /// <param name="insertGeneratedColumns"></param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <param name="afterConnectionQuery">Query for execute after connection (for example 'USE cp1251')</param>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>"
        /// <exception cref="ArgumentNullException">Invalid parameter</exception>"
        public void Insert(Entity cortege, bool insertGeneratedColumns = false, int timeOutSeconds = 30, string afterConnectionQuery = null)
        {
            if (cortege == null)
                throw new ArgumentNullException();

            var type = typeof(Entity);
            var propertyList = typeof(Entity).GetProperties().ToList();
            var customAttributeDictionary = new Dictionary<object, PropertyInfo>();
            var dbPropertyDictionaryForQueryBuilding = new Dictionary<Column, PropertyInfo>();

            foreach (var property in propertyList)
            {
                var dbProperty = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));

                if (dbProperty.Count == 0)
                    continue;

                customAttributeDictionary.Add(dbProperty[0], property);
            }

            foreach (var customProperty in customAttributeDictionary)
                dbPropertyDictionaryForQueryBuilding.Add((Column)customProperty.Key, customProperty.Value);

            var commandStringBuilder = new StringBuilder("INSERT INTO " + TableAddress + "(");

            for (var i = 0; i < dbPropertyDictionaryForQueryBuilding.Count; i++)
            {
                if (dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.IsGeneratedValueColumn && !insertGeneratedColumns)
                    continue;

                commandStringBuilder.Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name);

                if (i != dbPropertyDictionaryForQueryBuilding.Count - 1)
                    commandStringBuilder.Append(",");
            }

            commandStringBuilder.Append(")values(");

            for (var i = 0; i < dbPropertyDictionaryForQueryBuilding.Count; i++)
            {
                if (dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.IsGeneratedValueColumn && !insertGeneratedColumns)
                    continue;

                commandStringBuilder.Append("@");
                commandStringBuilder.Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name);

                if (i != dbPropertyDictionaryForQueryBuilding.Count - 1)
                    commandStringBuilder.Append(",");
            }

            commandStringBuilder.Append(")");

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                foreach (var dbProperty in dbPropertyDictionaryForQueryBuilding)
                {
                    object value = dbProperty.Value.GetValue(cortege);

                    if (dbProperty.Key.IsEnumObject)
                    {
                        if (dbProperty.Key.ConvertionMode == EnumConvertionMode.INT)
                            value = (int)dbProperty.Value.GetValue(cortege);
                        else
                            value = dbProperty.Value.GetValue(cortege).ToString();
                    }

                    command.Parameters.Add(new MySqlParameter(dbProperty.Key.Name, value));
                }

                try
                {
                    connection.Open();

                    if (afterConnectionQuery != null)
                        new MySqlCommand(afterConnectionQuery, connection).ExecuteNonQuery();

                    command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Updates the tuple fields in a table
        /// </summary>
        /// <param name="cortege">Object with new data</param>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="columns">Columns to update (default is null, i.e. all)</param>
        /// <param name="updateGeneratedColumn"></param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <param name="afterConnectionQuery">Query for executing after connection (for example 'USE cp1251')</param>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="ArgumentNullException">Invalid parameter</exception>
        public async void UpdateAsync(Entity cortege, CancellationToken cancellationToken, string[] columns = null, bool updateGeneratedColumn = false, int timeOutSeconds = 30, string afterConnectionQuery = null)
        {
            if (cortege == null)
                throw new ArgumentNullException("Cortege is null");

            var type = typeof(Entity);
            var propertyList = new List<PropertyInfo>();

            propertyList = typeof(Entity).GetProperties().ToList();

            var customAttributeDictionary = new Dictionary<object, PropertyInfo>();
            var dbPropertyDictionaryForQueryBuilding = new Dictionary<Column, PropertyInfo>();

            foreach (var property in propertyList)
            {
                var dbProperty = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));

                if (dbProperty.Count == 0 || (columns != null && !columns.Contains(((Column)dbProperty[0]).Name)))
                    continue;

                customAttributeDictionary.Add(dbProperty[0], property);
            }

            foreach (var customProperty in customAttributeDictionary)
                dbPropertyDictionaryForQueryBuilding.Add((Column)customProperty.Key, customProperty.Value);

            var primaryKeyPropertyDictionaryKeysList = dbPropertyDictionaryForQueryBuilding.Keys.ToList().FindAll(x => x.IsPrimaryKeyColumn);
            var primaryKeyPropertyDictionary = new Dictionary<Column, PropertyInfo>();

            foreach (var primaryKeyPropertyDictionaryKey in primaryKeyPropertyDictionaryKeysList)
                primaryKeyPropertyDictionary.Add(primaryKeyPropertyDictionaryKey, dbPropertyDictionaryForQueryBuilding[primaryKeyPropertyDictionaryKey]);

            if (!updateGeneratedColumn)
            {
                var generatedProperyKeysList = dbPropertyDictionaryForQueryBuilding.Keys.ToList().FindAll(x => x.IsGeneratedValueColumn);

                foreach (var generatedPropertyKey in generatedProperyKeysList)
                    dbPropertyDictionaryForQueryBuilding.Remove(generatedPropertyKey);
            }

            var commandStringBuilder = new StringBuilder("UPDATE " + TableAddress + " SET ");

            for (var i = 0; i < dbPropertyDictionaryForQueryBuilding.Count; i++)
            {
                if (dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.IsGeneratedValueColumn && !updateGeneratedColumn)
                    continue;

                commandStringBuilder
                    .Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name)
                    .Append("=@")
                    .Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name);

                if (i < dbPropertyDictionaryForQueryBuilding.Count - 1)
                    commandStringBuilder.Append(",");
            }

            for (var i = 0; i < primaryKeyPropertyDictionary.Count; i++)
            {
                var primaryKeyProperty = primaryKeyPropertyDictionary.ElementAt(i);

                if (i > 0)
                    commandStringBuilder.Append(" AND ");

                if (primaryKeyProperty.Value.GetValue(cortege).GetType() == typeof(string))
                    commandStringBuilder.Append(" WHERE " + primaryKeyProperty.Key.Name + " LIKE @" + primaryKeyProperty.Key.Name);
                else
                    commandStringBuilder.Append(" WHERE " + primaryKeyProperty.Key.Name + "=@" + primaryKeyProperty.Key.Name);
            }

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                foreach (var dbProperty in dbPropertyDictionaryForQueryBuilding)
                {
                    object value = dbProperty.Value.GetValue(cortege);

                    if (dbProperty.Key.IsEnumObject)
                    {
                        if (dbProperty.Key.ConvertionMode == EnumConvertionMode.INT)
                            value = (int)dbProperty.Value.GetValue(cortege);
                        else
                            value = dbProperty.Value.GetValue(cortege).ToString();
                    }

                    command.Parameters.Add(new MySqlParameter(dbProperty.Key.Name, value));
                }

                try
                {
                    await connection.OpenAsync(cancellationToken);

                    if (afterConnectionQuery != null)
                        new MySqlCommand(afterConnectionQuery, connection).ExecuteNonQuery();

                    await command.ExecuteNonQueryAsync(cancellationToken);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }
        }
        
        /// <summary>
        /// Updates the tuple fields in a table
        /// </summary>
        /// <param name="cortege">Object with new data</param>
        /// <param name="columns">Columns to update (default is null, i.e. all)</param>
        /// <param name="updateGeneratedColumn"></param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <param name="afterConnectionQuery">Query for executing after connection (for example 'USE cp1251')</param>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        /// <exception cref="ArgumentNullException">Invalid parameter</exception>
        public void Update(Entity cortege, string[] columns = null, bool updateGeneratedColumn = false, int timeOutSeconds = 30, string afterConnectionQuery = null)
        {
            if (cortege == null)
                throw new ArgumentNullException("Cortege is null");

            var type = typeof(Entity);
            var propertyList = new List<PropertyInfo>();

            propertyList = typeof(Entity).GetProperties().ToList();

            var customAttributeDictionary = new Dictionary<object, PropertyInfo>();
            var dbPropertyDictionaryForQueryBuilding = new Dictionary<Column, PropertyInfo>();

            foreach (var property in propertyList)
            {
                var dbProperty = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));

                if (dbProperty.Count == 0 || (columns != null && !columns.Contains(((Column)dbProperty[0]).Name) && !((Column)dbProperty[0]).IsPrimaryKeyColumn))
                    continue;

                customAttributeDictionary.Add(dbProperty[0], property);
            }

            foreach (var customProperty in customAttributeDictionary)
                dbPropertyDictionaryForQueryBuilding.Add((Column)customProperty.Key, customProperty.Value);

            var primaryKeyPropertyDictionaryKeysList = dbPropertyDictionaryForQueryBuilding.Keys.ToList().FindAll(x => x.IsPrimaryKeyColumn);
            var primaryKeyPropertyDictionary = new Dictionary<Column, PropertyInfo>();

            foreach (var primaryKeyPropertyDictionaryKey in primaryKeyPropertyDictionaryKeysList)
                primaryKeyPropertyDictionary.Add(primaryKeyPropertyDictionaryKey, dbPropertyDictionaryForQueryBuilding[primaryKeyPropertyDictionaryKey]);

            var commandStringBuilder = new StringBuilder("UPDATE " + TableAddress + " SET ");

            for (var i = 0; i < dbPropertyDictionaryForQueryBuilding.Count; i++)
            {
                if (dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.IsGeneratedValueColumn && !updateGeneratedColumn)
                    continue;

                commandStringBuilder
                    .Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name)
                    .Append("=@")
                    .Append(dbPropertyDictionaryForQueryBuilding.ElementAt(i).Key.Name);

                if (i < dbPropertyDictionaryForQueryBuilding.Count - 1)
                    commandStringBuilder.Append(",");
            }

            for (var i = 0; i < primaryKeyPropertyDictionary.Count; i++)
            {
                var primaryKeyProperty = primaryKeyPropertyDictionary.ElementAt(i);

                if (i > 0)
                    commandStringBuilder.Append(" AND ");

                if (primaryKeyProperty.Value.GetValue(cortege).GetType() == typeof(string))
                    commandStringBuilder.Append(" WHERE " + primaryKeyProperty.Key.Name + " LIKE @" + primaryKeyProperty.Key.Name);
                else
                    commandStringBuilder.Append(" WHERE " + primaryKeyProperty.Key.Name + "=@" + primaryKeyProperty.Key.Name);
            }

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandStringBuilder.ToString(), connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                foreach (var dbProperty in dbPropertyDictionaryForQueryBuilding)
                {
                    object value = dbProperty.Value.GetValue(cortege);

                    if (dbProperty.Key.IsEnumObject)
                    {
                        if (dbProperty.Key.ConvertionMode == EnumConvertionMode.INT)
                            value = (int)dbProperty.Value.GetValue(cortege);
                        else
                            value = dbProperty.Value.GetValue(cortege).ToString();
                    }

                    command.Parameters.Add(new MySqlParameter(dbProperty.Key.Name, value));
                }

                try
                {
                    connection.Open();

                    if (afterConnectionQuery != null)
                        new MySqlCommand(afterConnectionQuery, connection).ExecuteNonQuery();

                    command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }
        }

        private string BuildAttributesSelectString(string[] columns)
        {
            if (columns.Length == 0)
                throw new ArgumentNullException("List of columns can not be empty");

            var columnsStringBuilder = new StringBuilder();

            for (var i = 0; i < columns.Length; i++)
            {
                columnsStringBuilder.Append(columns[i]);

                if (i != columns.Length - 1)
                    columnsStringBuilder.Append(",");
            }

            return columnsStringBuilder.ToString();
        }

        private List<Entity> BindEntityDataList(IDataReader dataReader, string[] columns)
        {
            if (dataReader == null)
                throw new ArgumentNullException();

            var type = typeof(Entity);
            var propertyList = typeof(Entity).GetProperties().ToList();
            var customAttributeDictionary = new Dictionary<object, PropertyInfo>();
            var dbPropertyDictionary = new Dictionary<Column, PropertyInfo>();
            var entityList = new List<Entity>(dataReader.FieldCount);

            foreach (var property in propertyList)
            {
                var dbProperty = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));

                if (dbProperty.Count == 0 || (columns != null && !columns.Contains(((Column)dbProperty[0]).Name)))
                    continue;

                customAttributeDictionary.Add(dbProperty[0], property);
            }

            foreach (var customProperty in customAttributeDictionary)
                dbPropertyDictionary.Add((Column)customProperty.Key, customProperty.Value);

            while (dataReader.Read())
            {
                var entity = new Entity();

                foreach (var dbProperty in dbPropertyDictionary)
                {
                    if (dataReader[dbProperty.Key.Name].GetType() == typeof(DBNull))
                        continue;

                    var value = dataReader[dbProperty.Key.Name];

                    if (dbProperty.Key.IsEnumObject)
                    {
                        if (dbProperty.Key.ConvertionMode == EnumConvertionMode.INT)
                            value = Enum.ToObject(dbProperty.Key.EnumType, (int)dataReader[dbProperty.Key.Name]);
                        else
                            value = Enum.Parse(dbProperty.Key.EnumType, (string)dataReader[dbProperty.Key.Name]);
                    }

                    type.GetProperty(dbProperty.Value.Name).SetValue(entity, value);
                }

                entityList.Add(entity);
            }

            return entityList;
        }

        private Entity BindEntityData(IDataReader dataReader, string[] columns)
        {
            if (dataReader == null)
                throw new ArgumentNullException();

            var type = typeof(Entity);
            var propertyList = typeof(Entity).GetProperties().ToList();
            var customAttributeDictionary = new Dictionary<object, PropertyInfo>();
            var dbPropertyDictionary = new Dictionary<Column, PropertyInfo>();
            var entity = new Entity();

            foreach (var property in propertyList)
            {
                var dbProperty = property.GetCustomAttributes(false).ToList().FindAll(x => x.GetType() == typeof(Column));

                if (dbProperty.Count == 0 || (columns != null && !columns.Contains(((Column)dbProperty[0]).Name)))
                    continue;

                customAttributeDictionary.Add(dbProperty[0], property);
            }

            foreach (var customProperty in customAttributeDictionary)
                dbPropertyDictionary.Add((Column)customProperty.Key, customProperty.Value);

            dataReader.Read();

            foreach (var dbProperty in dbPropertyDictionary)
            {
                if (dataReader[dbProperty.Key.Name].GetType() == typeof(DBNull))
                    continue;

                var value = dataReader[dbProperty.Key.Name];

                if (dbProperty.Key.IsEnumObject)
                {
                    if (dbProperty.Key.ConvertionMode == EnumConvertionMode.INT)
                        value = Enum.ToObject(dbProperty.Key.EnumType, (int)dataReader[dbProperty.Key.Name]);
                    else
                        value = Enum.Parse(dbProperty.Key.EnumType, (string)dataReader[dbProperty.Key.Name]);
                }

                type.GetProperty(dbProperty.Value.Name).SetValue(entity, value);
            }

            return entity;
        }
    }
}
