﻿using System;

namespace MySqlDbController
{
    /// <summary>
    /// The attribute that describes the table. Designed for public classes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    {
        /// <summary>
        /// DB table address [dbname.tablename]
        /// </summary>
        public string TableAddress { get; private set; }

        /// <summary>
        /// Attribute constructor
        /// </summary>
        /// <param name="name">DB table name</param>
        public Table(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException();

            TableAddress = name;
        }
    }

    /// <summary>
    /// An attribute that describes a column in a DB table. Designed for fully public properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
#pragma warning disable CS0659 // Тип переопределяет Object.Equals(object o), но не переопределяет Object.GetHashCode()
    public class Column : Attribute
#pragma warning restore CS0659 // Тип переопределяет Object.Equals(object o), но не переопределяет Object.GetHashCode()
    {
        /// <summary>
        /// Column name in DB table
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Associated property - primary key
        /// </summary>
        public bool IsPrimaryKeyColumn { get; private set; }

        /// <summary>
        /// Indicate that column is auto increment or generated
        /// </summary>
        public bool IsGeneratedValueColumn { get; private set; }

        /// <summary>
        /// Associated property - enumeration
        /// </summary>
        public bool IsEnumObject { get; private set; }

        /// <summary>
        /// Type of enumeration
        /// </summary>
        public Type EnumType { get; private set; }

        /// <summary>
        /// Enumeration conversion method
        /// </summary>
        public EnumConvertionMode ConvertionMode { get; private set; }

        /// <summary>
        /// Attribute constructor
        /// </summary>
        /// <param name="name">Column name in DB table</param>
        /// <param name="isPrimaryKeyColumn">Associated property - primary key</param>
        /// <param name="isGeneratedValueColumn">Indicate that column is auto increment or generated</param>
        /// <param name="isEnumObject">Associated property - enumeration</param>
        /// <param name="enumType">Type of enumeration</param>
        /// <param name="convertionMode">Enumeration conversion method</param>
        public Column(
            string name, 
            bool isPrimaryKeyColumn = false, 
            bool isGeneratedValueColumn = false, 
            bool isEnumObject = false, 
            Type enumType = null, 
            EnumConvertionMode convertionMode = EnumConvertionMode.INT)
        {
            Name = name;
            IsPrimaryKeyColumn = isPrimaryKeyColumn;
            IsGeneratedValueColumn = isGeneratedValueColumn;
            IsEnumObject = isEnumObject;
            EnumType = enumType;
            ConvertionMode = convertionMode;
        }

#pragma warning disable CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена
        public override bool Equals(object obj)
#pragma warning restore CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена
        {
            var forEqualsObject = (Column)obj;

            return Name.Equals(forEqualsObject.Name);
        }
    }

    /// <summary>
    /// Column settings attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnSettings : Attribute
    {
        /// <summary>
        /// Column data type
        /// </summary>
        public ColumnDataType DataType { get; private set; }

        /// <summary>
        /// Column content length
        /// </summary>
        public uint DataTypeLenght { get; private set; }

        /// <summary>
        /// Default value
        /// </summary>
        public object DefaultValue { get; private set; }

        /// <summary>
        /// Cannot be null
        /// </summary>
        public bool NotNull { get; private set; }

        /// <summary>
        /// Auto increment value
        /// </summary>
        public bool AutoIncrement { get; private set; }

        /// <summary>
        /// Primary key column
        /// </summary>
        public bool PrimaryKey { get; set; }

        /// <summary>
        /// Attribute constructor
        /// </summary>
        /// <param name="dataType">Column data type</param>
        /// <param name="dataTypeLenght">Column content length</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="notNull">Cannot be null</param>
        /// <param name="autoIncrement">Auto increment value</param>
        /// <param name="primaryKey">Primary key column</param>
        public ColumnSettings(ColumnDataType dataType, uint dataTypeLenght = 0, object defaultValue = null, bool notNull = false, bool autoIncrement = false, bool primaryKey = false)
        {
            DataType = dataType;
            DataTypeLenght = dataTypeLenght;
            DefaultValue = defaultValue;
            NotNull = notNull;
            AutoIncrement = autoIncrement;
            PrimaryKey = primaryKey;

            if (notNull == true && autoIncrement == true)
                throw new ArgumentException("NotNull and AutoIncrement parameters cannot be TRUE at the same time");
        }
    }

    /// <summary>
    /// Enumeration conversion method
    /// </summary>
    public enum EnumConvertionMode
    {
        /// <summary>
        /// Store enum value like integer value
        /// </summary>
        INT = 0,

        /// <summary>
        /// Store enum value like string
        /// </summary>
        STRING = 1
    }

    /// <summary>
    /// Column data type
    /// </summary>
    public enum ColumnDataType
    {
#pragma warning disable CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена
        TINYINT = 0,
        SMALLINT = 1,
        INT = 2,
        BIGINT = 3,
        DECIMAL = 4,
        FLOAT = 4,
        DOUBLE = 5,
        TINYTEXT = 6,
        MEDIUMTEXT = 7,
        TEXT = 8,
        LONGTEXT = 9,
        DATE = 10,
        TIME = 11,
        DATETIME = 12,
        TINYBLOB = 13,
        MEDIUMBLOB = 14,
        BLOB = 15,
        LONGBLOB = 16,
        CHAR = 17,
        VARCHAR = 18
#pragma warning restore CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена
    }
}
