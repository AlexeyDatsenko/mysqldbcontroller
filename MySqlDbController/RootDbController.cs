﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MySqlDbController
{
    /// <summary>
    /// DB controller without type parameters
    /// </summary>
    public class RootDbController
    {
        /// <summary>
        /// Connection to DB
        /// </summary>
        public MySqlConnection Connection { get; private set; }

        /// <summary>
        /// DB Connection string
        /// </summary>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Parameters for connection to DB
        /// </summary>
        public ConnectionSettings Settings { get; private set; }

        /// <summary>
        /// Simple manual constructor
        /// </summary>
        /// <param name="host">Server address</param>
        /// <param name="login">Username</param>
        /// <param name="password">Password</param>
        public RootDbController(
            string host,
            string login,
            string password)
        {
            Settings = new ConnectionSettings(host, login, password);
            Connection = new MySqlConnection(BuildConnectionString());
        }

        /// <summary>
        /// Constructor from settings object
        /// </summary>
        /// <param name="connectionSettings">Connection parameters</param>
        public RootDbController(ConnectionSettings connectionSettings)
        {
            Settings = connectionSettings;
            Connection = new MySqlConnection(BuildConnectionString());
        }

        /// <summary>
        /// MySQL constructor
        /// </summary>
        /// <param name="mySqlConnection">MySQL connection</param>
        public RootDbController(MySqlConnection mySqlConnection) { Connection = mySqlConnection; }

        /// <summary>
        /// Constructor from file
        /// </summary>
        /// <param name="connectionName">Connection identifier</param>
        /// <param name="connectionFilePath">File path</param>
        public RootDbController(string connectionName, string connectionFilePath)
        {
            if (connectionName == null || connectionName.Length == 0)
                throw new NullReferenceException("Connection name must not null or empty.");

            if (connectionFilePath == null || connectionFilePath.Length == 0)
                throw new NullReferenceException("Connection file must not null or empty.");

            var regex = new Regex("(?<name>\".+?\"):(?<connectionString>\".+?\")", RegexOptions.IgnoreCase);
            var lines = File.ReadAllLines(connectionFilePath, Encoding.UTF8);

            foreach (var line in lines)
            {
                var match = regex.Match(line);

                if (match.Success)
                {
                    if (connectionName != null)
                    {
                        if (match.Groups["name"].Value.Trim('"') == connectionName)
                        {
                            Connection = new MySqlConnection(match.Groups["connectionString"].Value.Trim('"'));
                            break;
                        }
                    }
                    else
                    {
                        Connection = new MySqlConnection(match.Groups["connectionString"].Value.Trim('"'));
                        break;
                    }
                }
            }

            if (Connection == null)
                throw new Exception($"Can not found connection name \"{connectionName}\" in file \"{connectionFilePath}\"");
        }

        /// <summary>
        /// Checks DB Connection
        /// </summary>
        public bool CheckConnection()
        {
            using (var connection = (MySqlConnection)Connection.Clone())
            {
                try
                {
                    connection.Open();

                    return true;
                }
                catch (MySqlException)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Create data base if it not exists
        /// </summary>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>"
        public void CreateDbIfNotExists()
        {
            var commandText = "CREATE DATABASE IF NOT EXISTS " + Connection.Database;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection);

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }
        }

        /// <summary>
        /// Gets a list of databases
        /// </summary>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>List of DB's</returns>
        /// <exception cref="MySqlException">Error of connection or executeting command</exception>
        public List<string> GetDataBases(int timeOutSeconds = 30)
        {
            var commandString = "SHOW DATABASES";

            var resultList = new List<string>();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandString, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    connection.Open();

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                        resultList.Add((string)dataReader[0]);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return resultList;
        }

        /// <summary>
        /// Gets a list of tables in the DB
        /// </summary>
        /// <param name="dataBaseName">DB Name</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>Table name list</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        public List<string> GetTables(string dataBaseName, int timeOutSeconds = 30)
        {
            var commandString = "USE " + dataBaseName + ";SHOW TABLES";

            var resultList = new List<string>();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandString, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    connection.Open();

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                        resultList.Add((string)dataReader[0]);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return resultList;
        }

        /// <summary>
        /// Creates a file with a public class, fully public properties, a default constructor, attributes for working with this library.
        /// </summary>
        /// <param name="destinationPath">Target file path</param>
        /// <param name="tableAddress">[db_name].[table_name]</param>
        /// <param name="nameSpace">Namespace</param>
        /// <param name="includeManualConstructor">Include manual constructor</param>
        /// <exception cref="Exception">Exception in general form</exception>
        public void CreateClassFile(string destinationPath, string tableAddress, string nameSpace, bool includeManualConstructor = false)
        {
            var classStringBuilder = new StringBuilder();
            var propertyDataList = GetPropertiesByColumns(tableAddress);
            var className = MySqlNameToSharpName(TableAddressToClassName(tableAddress));

            classStringBuilder
                .Append("using System;\nusing MySqlDbController;\n\nnamespace ")
                .Append(nameSpace)
                .Append("\n");

            classStringBuilder
                .Append("{\n\t")
                .Append("[Table(\"")
                .Append(tableAddress)
                .Append("\")]\n\tpublic class ")
                .Append(className)
                .Append("\n\t{\n");

            foreach (var property in propertyDataList)
            {
                classStringBuilder
                    .Append("\t\t[Column(\"")
                    .Append(property.Field);

                if (property.IsPrimaryKey)
                    classStringBuilder.Append(", isPrimaryKeyColumn: true");

                if (property.AutoIncrement)
                    classStringBuilder.Append(", isGeneratedValueColumn: true");

                classStringBuilder
                    .Append("\")]\n\t\t")
                    .Append("public ")
                    .Append(MySqlTypeToSharpType(property.Type))
                    .Append(" ")
                    .Append(MySqlNameToSharpName(property.Field))
                    .Append(" { get; set; }\n\n");
            }

            classStringBuilder
                .Append("\t\tpublic ")
                .Append(className)
                .Append("() { }\n");

            if (includeManualConstructor)
            {
                classStringBuilder
                    .Append("\n\t\tpublic ")
                    .Append(className)
                    .Append("(");

                for (var i = 0; i < propertyDataList.Count; i++)
                {
                    var property = propertyDataList.ElementAt(i);

                    classStringBuilder
                        .Append(MySqlTypeToSharpType(property.Type))
                        .Append(" ")
                        .Append(MySqlNameToSharpName(property.Field, false));

                    if (i != propertyDataList.Count - 1)
                        classStringBuilder.Append(", ");
                }

                classStringBuilder
                    .Append(") : base(id)\n\t\t{\n");

                foreach (var property in propertyDataList)
                {
                    classStringBuilder
                        .Append("\t\t\t")
                        .Append(MySqlNameToSharpName(property.Field))
                        .Append(" = ")
                        .Append(MySqlNameToSharpName(property.Field, false))
                        .Append(";\n");
                }

                classStringBuilder
                    .Append("\t\t}");
            }

            classStringBuilder
                .Append("\n\t}\n}");

            try
            {
                using (var fileStream = File.Create(destinationPath))
                    using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                        streamWriter.Write(classStringBuilder.ToString());
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Escapes the string
        /// </summary>
        /// <param name="inputString">Input string</param>
        /// <returns>Output string</returns>
        public static string EscapeString(string inputString)
        {
            if (!string.IsNullOrEmpty(inputString))
                return MySqlHelper.EscapeString(inputString);

            return inputString;
        }

        /// <summary>
        /// Executes a SELECT request
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>SELECT request result</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        public DataTable SelectByQuery(string commandText, int timeOutSeconds = 30)
        {
            var dataTable = new DataTable();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                var adapter = new MySqlDataAdapter(command);

                try
                {
                    connection.Open();
                    adapter.Fill(dataTable);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Executes a SELECT request asynchronously
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>SELECT request result</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        public async Task<DataTable> SelectByQueryAsync(string commandText, CancellationToken cancellationToken, int timeOutSeconds = 30)
        {
            var dataTable = new DataTable();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                var adapter = new MySqlDataAdapter(command);

                try
                {
                    await connection.OpenAsync(cancellationToken);
                    await adapter.FillAsync(dataTable, cancellationToken);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Executes a SELECT request (one row)
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>SELECT request result (one row)</returns>
        public DataRow SelectOneRow(string commandText, int timeOutSeconds = 30)
        {
            var dataTable = new DataTable();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                var adapter = new MySqlDataAdapter(command);

                try
                {
                    connection.Open();
                    adapter.Fill(dataTable);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            if (dataTable.Rows.Count != 0)
                return dataTable.Rows[0];
            else
                return null;
        }

        /// <summary>
        /// Executes a SELECT request (one row) asynchronously
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>SELECT request result (one row)</returns>
        public async Task<DataRow> SelectOneRowAsync(string commandText, CancellationToken cancellationToken, int timeOutSeconds = 30)
        {
            var dataTable = new DataTable();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                var adapter = new MySqlDataAdapter(command);

                try
                {
                    await connection.OpenAsync(cancellationToken);
                    await adapter.FillAsync(dataTable);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            if (dataTable.Rows.Count != 0)
                return dataTable.Rows[0];
            else
                return null;
        }

        /// <summary>
        /// Executes a query without an explicit result
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>Number of rows affected</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        public int ExecuteNonQuery(string commandText, int timeOutSeconds = 30)
        {
            var count = 0;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    connection.Open();
                    count = command.ExecuteNonQuery();
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return count;
        }

        /// <summary>
        /// Executes a query without an explicit result asynchronously
        /// </summary>
        /// <param name="commandText">Request text</param>
        /// <param name="cancellationToken">Token for cancel</param>
        /// <param name="timeOutSeconds">Maximum request time in seconds</param>
        /// <returns>Number of rows affected</returns>
        /// <exception cref="MySqlException">Error of connection or executing command</exception>
        public async Task<int> ExecuteNonQueryAsync(string commandText, CancellationToken cancellationToken, int timeOutSeconds = 30)
        {
            var count = 0;

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandText, connection)
                {
                    CommandTimeout = timeOutSeconds
                };

                try
                {
                    await connection.OpenAsync(cancellationToken);
                    count = await command.ExecuteNonQueryAsync(cancellationToken);
                }
                catch (MySqlException exception)
                {
                    throw exception;
                }
            }

            return count;
        }

#pragma warning disable CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена

        protected string BuildConnectionString()
        {
            ConnectionString =
                "Database=" + Settings.DataBase + ";" +
                "Server=" + Settings.Host +
                ";Port=" + Settings.Port.ToString() +
                ";User=" + Settings.Login +
                ";Password=" + Settings.Password +
                ";CharSet=" + Settings.CharSet +
                ";SslMode=" + Settings.SslMode +
                ";AllowPublicKeyRetrieval=" + Settings.AllowPublicKeyRetrieval.ToString() +
                ";AllowUserVariables=" + Settings.AllowUserVariables.ToString() + 
                ";Convert Zero Datetime=" + Settings.ConvertZeroDateTime.ToString() + ";";

            return ConnectionString;
        }

        protected List<ColumnData> GetPropertiesByColumns(string tableName)
        {
            var commandString = "DESCRIBE " + tableName;

            var resultList = new List<ColumnData>();

            using (var connection = (MySqlConnection)Connection.Clone())
            {
                var command = new MySqlCommand(commandString, connection);

                try
                {
                    connection.Open();
                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                        resultList.Add(new ColumnData(dataReader));
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return resultList;
        }

        protected string TableAddressToClassName(string tableAddress)
        {
            var regexPattern = "\\w+\\.";
            var regex = new Regex(regexPattern);
            return regex.Replace(tableAddress, string.Empty);
        }

        protected string MySqlNameToSharpName(string mySqlName, bool firstCharacterUpper = true)
        {
            var resultStringBuilder = new StringBuilder();

            var regexString = @"_?[A-Za-z0-9]+_?";
            var regex = new Regex(regexString);
            var matchCollection = regex.Matches(mySqlName);

            foreach (Match match in matchCollection)
                resultStringBuilder.Append(match.Value.First().ToString().ToUpper() + match.Value.Replace("_", string.Empty).Substring(1));

            var result = resultStringBuilder.ToString();

            if (!firstCharacterUpper)
                result = result.First().ToString().ToLower() + result.Substring(1);

            return result;
        }

        protected string MySqlTypeToSharpType(string mySqlType)
        {
            if (mySqlType.Equals("tinyint(1)"))
                return "bool";

            var regexString = @"\(\d+\)";
            var regex = new Regex(regexString);
            var cleanType = regex.Replace(mySqlType, string.Empty).ToLower();

            switch (cleanType)
            {
                case "int":
                    return "int";
                case "varchar":
                    return "string";
                case "char":
                    return "string";
                case "tinytext":
                    return "string";
                case "mediumtext":
                    return "string";
                case "text":
                    return "string";
                case "longtext":
                    return "string";
                case "tinyint":
                    return "byte";
                case "smallint":
                    return "short";
                case "mediumint":
                    return "int";
                case "bigint":
                    return "long";
                case "tinyblob":
                    return "byte[]";
                case "mediumblob":
                    return "byte[]";
                case "blob":
                    return "byte[]";
                case "longblob":
                    return "byte[]";
                case "datetime":
                    return "DateTime";
                case "time":
                    return "TimeSpan";
                default:
                    return "object";
            }
        }
    }

    public class ColumnData
    {
        public string Field { get; private set; }
        public string Type { get; private set; }
        public bool IsPrimaryKey { get; private set; } = false;
        public bool AutoIncrement { get; set; } = false;

        public ColumnData(IDataReader dataReader)
        {
            Field = (string)dataReader[0];
            Type = (string)dataReader[1];
            var tmpPK = (string)dataReader[2];
            var tmpExtra = (string)dataReader[3];

            if (tmpPK.ToUpper().Equals("PRI"))
                IsPrimaryKey = true;

            if (tmpExtra.ToUpper().Equals("AUTO_INCREMENT"))
                AutoIncrement = true;
        }
    }

#pragma warning restore CS1591 // Отсутствует комментарий XML для открытого видимого типа или члена
}
