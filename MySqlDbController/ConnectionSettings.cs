﻿namespace MySqlDbController
{
    /// <summary>
    /// Contains parameters for connection to DB
    /// </summary>
    public class ConnectionSettings
    {
        /// <summary>
        /// Server address
        /// </summary>
        public string Host { get; private set; }

        /// <summary>
        /// Port
        /// </summary>
        public uint Port { get; private set; }

        /// <summary>
        /// DB name
        /// </summary>
        public string DataBase { get; private set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Login { get; private set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Default encoding
        /// </summary>
        public string CharSet { get; private set; }

        /// <summary>
        /// SSL mode
        /// </summary>
        public string SslMode { get; private set; }

        /// <summary>
        /// Allow converting 00-00-00 00:00:00 DateTime query
        /// </summary>
        public bool ConvertZeroDateTime { get; private set; }

        /// <summary>
        /// Automatic server public key request
        /// </summary>
        public bool AllowPublicKeyRetrieval { get; private set; }

        /// <summary>
        /// Allow user variables
        /// </summary>
        public bool AllowUserVariables { get; private set; }

        /// <summary>
        /// Manual constructor
        /// </summary>
        /// <param name="host">Server address</param>
        /// <param name="port">Port</param>
        /// <param name="login">Username</param>
        /// <param name="password">Password</param>
        /// <param name="dataBase">Default DB</param>
        /// <param name="charSet">Default encoding</param>
        /// <param name="sslMode">SSL mode</param>
        /// <param name="convertZeroDateTime">Allow converting 00-00-00 00:00:00 DateTime query</param>
        /// <param name="allowPublicKeyRetrieval">Automatic server public key request</param>
        /// <param name="allowUserVariables">Allow user variables</param>
        public ConnectionSettings(
            string host,
            string login,
            string password,
            string dataBase = null,
            uint port = 3306,
            string charSet = "utf8",
            string sslMode = "none",
            bool convertZeroDateTime = true,
            bool allowPublicKeyRetrieval = true,
            bool allowUserVariables = true)
        {
            Host = host;
            Port = port;
            DataBase = dataBase;
            Login = login;
            Password = password;
            CharSet = charSet;
            SslMode = sslMode;
            ConvertZeroDateTime = convertZeroDateTime;
            AllowPublicKeyRetrieval = allowPublicKeyRetrieval;
            AllowUserVariables = allowUserVariables;
        }
    }
}
